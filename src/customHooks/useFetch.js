import {useEffect,useState} from 'react';
import axios from "axios"
import {API_URL} from "../constants"


const randomDate=_=> Date.now() - Math.floor(Math.random()*(60*1000*60*24*30))// random date of interval of 30days (because the api doesnt have a creation date prop)

export default function useFetch(url,params=null,refetchParam=null){
    const [data,setData]=useState([])
    const [isLoading,setIsLoading]=useState(false)
    useEffect(()=>{
        const fetchData=async(url)=>{
              setIsLoading(true)
            if(params){
                     let data=[]
                 for( let key of Object.keys(params)){
                    let {data:item} =await axios.get(API_URL+'/'+key)
                    data.push({product:item,...( params[key].qty &&  {quantity:params[key].qty} || {}  )})
                    }
                    setData(data)
                }
                else {let {data}=await axios.get(url);
                if(Array.isArray(data) && typeof data[0] !=="string") data.forEach(item=>item.createdAt=randomDate())
                 setData(data)
                     }
                  setIsLoading(false)   
                                }
        fetchData(url || API_URL)
        },[refetchParam || ''])
      return  [data,setData,isLoading]
}