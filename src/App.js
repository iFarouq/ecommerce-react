import './App.css';
import Navbar from './components/Navbar'
import {BrowserRouter} from 'react-router-dom'
import React from 'react'
import Routes from './Routes';


import {createStore} from 'redux'
import {Provider} from 'react-redux'
import shoppingCartReducer from "./reducers/shoppingCartReducer";


const store=createStore(shoppingCartReducer)

function App() {
  return (
    <Provider store={store}>

      <BrowserRouter>
                  <div className="app">
                              <Navbar/> 
                              <Routes/>
                  </div>
     </BrowserRouter> 

    </Provider>
     
    
  );
}

export default App;
