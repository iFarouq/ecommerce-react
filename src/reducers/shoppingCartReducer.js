import {REMOVE_PRODUCT,ADD_PRODUCT} from '../constants'


const saveToStorage=(data)=>sessionStorage.setItem("cartItems",JSON.stringify(data))

let sessionData=JSON.parse(sessionStorage.getItem("cartItems")) || {}
let total=0;
if(JSON.stringify(sessionData)!=="{}"){
for(let k in sessionData) total+=+sessionData[k].qty;
}
const initialState={shoppingCart:{items:{...sessionData},total}}

const shoppingCartReducer=(state=initialState,action)=>
{  switch (action.type) {
    case REMOVE_PRODUCT:
        {   let cart=state.shoppingCart.items;
            let id=action.payload.id
            state.shoppingCart.total-=cart[id].qty;
            delete cart[id];
            if(JSON.stringify(state.shoppingCart.items)==="{}") sessionStorage.clear("cartItems")
            else saveToStorage(state.shoppingCart.items)
            return state
        }
        break;
    case ADD_PRODUCT:
        {                      
                let cart=state.shoppingCart.items
                let id=action.payload.id,qty=+action.payload.qty;
             if(Object.values(cart).length===0){
                 state={...state,shoppingCart:{items:{[id]:{qty} },total:qty}}
                 saveToStorage(state.shoppingCart.items)
                 return state
             } 
             else{ 
                if(cart.hasOwnProperty(id)){
                      cart[id].qty+=+cart[id].qty;
                      state.shoppingCart.total+=qty;
                }
                else{
                      cart[id]={qty:qty};
                      state.shoppingCart.total+=qty;
                }
                    sessionStorage.setItem("cartItems",JSON.stringify(cart));
                    return state
                }             
        }
        break;
        default:
            return{...state}    
}

}
export default shoppingCartReducer
