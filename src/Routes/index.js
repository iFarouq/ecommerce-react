import Route from 'react-router-dom/Route'
import Home from '../containers/Home'
import ProductDetail from '../containers/ProductDetail'
import ShoppingCart from '../containers/ShoppingCart'


function Routes(){
    return (<>
                <Route path='/' exact>
                    <Home/>
                 </Route>
                 <Route path='/product/:id' render={props=><ProductDetail {...props}/>}>
                  
                 </Route>
                 <Route path='/cart' render={props=><ShoppingCart {...props}/>}>
                  
                 </Route>


                 </>
                 )
}

export default Routes