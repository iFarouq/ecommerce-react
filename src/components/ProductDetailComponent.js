import React,{useState} from 'react';
import './styles/ProductDetail.css';
import Input from './Input'
import Button from './Button'

import Card from './Card'
import getDate from '../utils/getDate'
 const ProductDetailComponent=({item,addToCart,suggestedItems,isLoading})=>{
     const [quantity,setQuantity]=useState(1)
     const handleChange = e=> {    let qt=e.target.value>0 && e.target.value<11?e.target.value:1
                                     setQuantity(qt);
                                 } 
      const handleAddToCart=_=>quantity>0 && addToCart(quantity);

      const calcPrice=()=>Math.floor(item.price*quantity*100)/100; // calculate total and keep only last two float digits                                                

      let content=<>  
                       <div className="prod-img">
                                 <img src={item.image}/>
                        </div>
                        <div className="detail">
                                 <h3>  {item.title}</h3>
                                <p>
                                    {item.description}
                                </p>
                                
                        </div>
                        <div className="right">
                            <div> <p> <strong>price: </strong>{ calcPrice()}$ </p> </div>
                            <div className="qt"> <span>quantity </span> <Input type="number" value={quantity} onChange={handleChange}/></div>
                            <div> <Button className="btn" onClick={handleAddToCart}>Add to cart</Button> </div>
                        </div>
                     </>
    return( 
         <>
        <div className="product-detail">
            {isLoading && <h2> Loading..</h2> ||  content}       
            </div>
            {!isLoading &&  <div className="suggestions">
                                    <h3><center>Suggested for you.</center></h3>
                                    <div className="body"> 
                                    {suggestedItems.map(({title:prodName,image,price,id,createdAt},i)=><Card id={id}  
                                                                                                            prodName={prodName}
                                                                                                             image={image}
                                                                                                              price={price} 
                                                                                                              createdAt={getDate(createdAt).join(' ')}
                                                                                                              key={i}/>)} 
                                    </div>
                            </div>
                         || '' }
           
            </>
    )
}
export default ProductDetailComponent
