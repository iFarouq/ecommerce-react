import React from 'react';
import {Link} from 'react-router-dom'

import './styles/Card.css';
import Button from './Button'

const Card=({id,prodName,image,price,addToCart,createdAt})=>{
    return(
        <div className="card">
        <div className="card-img">
        <Link className="view-product" to={"/product/"+id}><img src={image}/></Link>
        </div>
        <div className="details">
               <h3> {prodName}</h3>
               <span>{price}$</span>
               <div> <strong> created at : </strong>{(createdAt)}</div>
        </div>
        <div className="actions-card">
             {addToCart && <Button className="btn" onClick={_=>addToCart(id)}> Add to cart</Button>}
             <Button className="btn">
                        <Link to={"/product/"+id}>View Details</Link>
             </Button>
             
        </div>
    </div>
    )
}
export default Card