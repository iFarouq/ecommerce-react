import React,{useState} from 'react';
import {Link} from 'react-router-dom';
import './styles/CartItem.css';

import Input from "./Input"
import Button from "./Button"
const CartItem=({cartItem,setChange,index,deleteItem})=>{
    const [quantity,setQuantity]=useState(cartItem.quantity || 0)
    const HandleChange = e=> {    
                                  let qt=e.target.value>0 && e.target.value<11 && e.target.value !== quantity ?e.target.value:1
                                  console.log(qt)
                                  setQuantity(qt); 
                                  setChange(cartItem.product.id,qt)
                                }
   const calcPrice=()=>Math.floor(cartItem.product.price*quantity*100)/100; // calculate total and keep only last two float digits                                                                        
    return(
      <>
        <div className="cart" Key={index}>
        <div className="img">
         <Link to={"/product/"+cartItem.product.id}> <img src={cartItem.product.image}/>
         </Link>
        </div>
        <div className="name">
          <h3> {cartItem.product.title}</h3>
          <p>brand...</p>
        </div>
        <div className="price">
                <b>x{quantity}</b> {calcPrice() }$
               </div>
        <div className="quantity">
                <Input type="number" onChange={HandleChange} value={quantity} />
        </div>
        <div className="actions">
                <Button className="btn-danger" onClick={_=>deleteItem(cartItem.product.id)}>Remove product</Button>      
   
        </div>
       </div>
       </>
    )
}
export default CartItem