import React from 'react';
import './styles/Navbar.css'
import {useSelector} from "react-redux"
import cart_icon from "./styles/shopping-cart.svg"
import {Link} from "react-router-dom"
const Navbar=()=>{
    const total=useSelector(state=>state.shoppingCart.total)
    return(
        <div className="nav-bar">
                <div><h3><Link to="/">E-comm</Link></h3></div>
                <div className="link">
                    <ul>
                        <li>
                            <Link to="/cart"> 
                                <a  data-total={total} className="cart-icon"> 
                                     <img src={cart_icon} className="icon"></img>
                                </a>
                             </Link>
                        </li>
                    </ul>
                </div>
        </div>
    )
}
export default Navbar