import {ADD_PRODUCT,REMOVE_PRODUCT} from '../constants'

export const Add_To_Cart =(id,qty)=>({type:ADD_PRODUCT,payload:{id,qty}})
export const Remove_From_Cart =(id)=>({type:REMOVE_PRODUCT,payload:{id}})