const sortItems=(arr,criteria,type)=>{
    return arr.slice().sort((item1,item2)=>{
        if(type==="ASC") return item1[criteria]-item2[criteria]
        if(type==="DESC")  return item2[criteria]-item1[criteria]
        if(type==="OLDEST") return item1[criteria]-item2[criteria]
        if(type==="NEWEST") return item2[criteria]-item1[criteria]
        return 0;
          })
        }
 export default sortItems
