import React from 'react';
import {useSelector,useDispatch} from 'react-redux';

import './styles/ShoppingCart.css';
import CartItem from '../components/CartItem'
import {API_URL} from '../constants'
import useFetch from '../customHooks/useFetch';
import calcTotal from '../utils/calcTotal'
import {Remove_From_Cart} from '../actions'


const ShoppingCart=()=>{
      const sessionData=useSelector(state=>state.shoppingCart.items)
      const [cartItems,setCartitems]=useFetch(API_URL,sessionData)    
      const dispatch=useDispatch()
      const setChange=(id,qty)=>{
           let items=cartItems.slice().map(item=>item.product.id==id?(item.quantity=+qty,item):item)
           setCartitems(items)
      }
      const deleteItem=(id)=>{setCartitems(cartItems.filter(({product})=>product.id!==id));dispatch(Remove_From_Cart(id));
                                        }
    return(   <>
             <div className="shopping-cart">
                        <h2> Shopping Cart.</h2>
                         {JSON.stringify(sessionData)!=="{}" && !cartItems.length && <div className="loading"><h2>Loading...</h2></div>}

                         {JSON.stringify(sessionData)=="{}" &&  <div className="loading"><h2>your cart is empty</h2>
                                                                     <a href="/"> Add some products?</a>
                                                                            </div>}

                          { cartItems.map((item,i)=><CartItem key={item.product.id} cartItem={item} setChange={setChange} index={i} deleteItem={deleteItem}/>)  }
                        <div className="total"><h3>Sub-total : {(cartItems.length && calcTotal(cartItems)+"$") || "0.00$"}</h3></div>
                        </div>
                        </>
            
    )
}
export default ShoppingCart