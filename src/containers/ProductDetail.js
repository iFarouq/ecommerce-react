import React from 'react';
import '../components/styles/ProductDetail.css';
import {useDispatch} from 'react-redux'
import useFetch from '../customHooks/useFetch';
import {API_URL} from '../constants';
import {Add_To_Cart} from "../actions"
import ProductDetailComponent from '../components/ProductDetailComponent'
import sliceRandom from "../utils/sliceRandom" 




const ProductDetail=({match})=>{
    const [item, ,isLoading]=useFetch(API_URL+"/"+match.params.id,null,match.params.id)
     const [suggestedItems]=useFetch(API_URL)
    const dispatch=useDispatch()
     const addToCart=quantity=> quantity>0 && (dispatch(Add_To_Cart(match.params.id,quantity)),alert("product added to your cart"))                           

    return( Object.values(item).length && <ProductDetailComponent item={item} addToCart={addToCart} 
                            suggestedItems={sliceRandom(suggestedItems.filter(({id})=>+id!==+match.params.id),4)}
                             isLoading={isLoading}/>)
}
export default ProductDetail