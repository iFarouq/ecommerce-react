import React,{useState,useEffect} from 'react';
import {useDispatch} from 'react-redux'
import useFetch from '../customHooks/useFetch'
import './styles/Home.css';
import Card from '../components/Card';
import {API_URL} from '../constants';
import sortItems from "../utils/sort"
import {Add_To_Cart} from "../actions"
import Select from "../components/Select";
import getDate from "../utils/getDate";
import {criterias} from '../constants';


const Home=()=>{
    const [items,setItems]=useFetch(API_URL);
    const [filteredItems,setFilteredItems]=useState([])
    const [categories]=useFetch(API_URL+"/categories")
    const dispatch = useDispatch();

    const handleSort = e=>{
       if(!e.target.value) {setFilteredItems([]);return}
       if(!filteredItems.length){
        let sortedItems=sortItems(items,criterias[e.target.value],e.target.value)
        setItems(sortedItems)
                    }
        else{
            let sortedItems=sortItems(filteredItems,criterias[e.target.value],e.target.value)
            setFilteredItems(sortedItems)
            }
                        }

    const handleFilter= e=>{
        let filter_by=e.target.value;
        if(!filter_by) {setFilteredItems([]);return}

        let filteredItems=items.slice().filter(({category})=>category===filter_by)
        setFilteredItems(filteredItems)
    }                    

    const addItemToCart=(id)=>{
        dispatch(Add_To_Cart(id,1))
        alert("product added to your cart")     
                            }

    const renderItems=()=>((filteredItems.length && filteredItems) || items).map(({title:prodName,image,price,id,createdAt},i)=><Card  Key={i} prodName={prodName} 
                                                                                 price={price} image={image}
                                                                                  price={price} 
                                                                                   id={id} 
                                                                                   createdAt={getDate(createdAt).join(" ")}
                                                                                   addToCart={addItemToCart}
                                                                                   />)                        
    return(<div className="home-page">  
                     <div className="actions-home">
                                    <Select options={[{value:"ASC",text:"sort by price : ASC"},
                                                        {value:"DESC",text:"sort by price : DESC"},
                                                        {value:"DESC",text:"sort by : newests"},
                                                        {value:"ASC",text:"sort by : oldest"}
                                                    
                                                       ]} 
                                                                        onChange={handleSort} 
                                                                        defaultOption="Sort items" />
                                     <Select options={categories.map(category=>({value:category,text:category}))}  
                                                                        defaultOption="Our categories"
                                                                        onChange={handleFilter}/>
                          </div>
                 
                    <div className="container">
                     {  renderItems()  }  
                        </div>
                </div>
    )
}
export default Home
